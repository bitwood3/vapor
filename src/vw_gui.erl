%%% @doc
%%% Vapor GUI
%%%
%%% This process is responsible for creating the main GUI frame displayed to the user.
%%%
%%% Reference: http://erlang.org/doc/man/wx_object.html
%%% @end

-module(vw_gui).
-vsn("0.2.4").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-behavior(wx_object).
-include_lib("wx/include/wx.hrl").
-export([show/2, user_message/1, update_versions/2]).
-export([start_link/2]).
-export([init/1, terminate/2, code_change/3,
         handle_call/3, handle_cast/2, handle_info/2, handle_event/2]).
-include("$zx_include/zx_logger.hrl").


-record(s,
        {wx         = none :: none | wx:wx_object(),
         frame      = none :: none | wx:wx_object(),
         menu       = none :: none | wx:wx_object(),
         main       = none :: none | wx:wx_object(),
         apps       = none :: none | wx:wx_object(),
         previous   = []   :: [zx:package_id()],
         packages   = []   :: [package()]}).

-record(package,
        {id         = none :: none | zx:package_id(),
         launch_id  = none :: none | integer(),
         vsn_picker = none :: none | vsn_picker(),
         vsn_button = none :: none | wx:wx_object()}).


-type state()      :: #s{}.
-type package()    :: #package{}.
-type vsn_picker() :: {button, integer()} | wx:wx_object().




%%% Labels

-define(fileIMP_REALM, 21).
-define(fileEXP_REALM, 22).
-define(fileREM_REALM, 23).
-define(toolOBSERVER, 31).


%%% Interface functions

show(Type, Descriptions) ->
    wx_object:cast(?MODULE, {show, Type, Descriptions}).


user_message(Contents) ->
    wx_object:cast(?MODULE, {user_message, Contents}).


update_versions(PackageID, Versions) ->
    wx_object:cast(?MODULE, {update_versions, PackageID, Versions}).


%%% Startup Functions

start_link(Title, Previous) ->
    wx_object:start_link({local, ?MODULE}, ?MODULE, {Title, Previous}, []).


init({Title, Previous}) ->
    ok = log(info, "GUI starting..."),
    Wx = wx:new(),
    Frame = wxFrame:new(Wx, ?wxID_ANY, Title),

    MB = wxMenuBar:new(),
    File = wxMenu:new([]),
    Tool = wxMenu:new([]),
    Help = wxMenu:new([]),
    _ = wxMenu:append(File, ?fileIMP_REALM, "Import Realm"),
    _ = wxMenu:append(File, ?fileEXP_REALM, "Export Realm"),
    _ = wxMenu:append(File, ?fileREM_REALM, "Remove Realm"),
    _ = wxMenu:append(File, ?wxID_EXIT, "&Quit"),
    _ = wxMenu:append(Tool, ?toolOBSERVER, "Start Observer"),
    _ = wxMenu:append(Help, ?wxID_HELP, "&Help"),
    _ = wxMenu:append(Help, ?wxID_ABOUT, "About"),
    _ = wxMenuBar:append(MB, File, "&File"),
    _ = wxMenuBar:append(MB, Tool, "&Tool"),
    _ = wxMenuBar:append(MB, Help, "&Help"),
    ok = wxFrame:setMenuBar(Frame, MB),

    MainSz = wxBoxSizer:new(?wxVERTICAL),
    AppWn  = wxScrolledWindow:new(Frame),
    _ = wxBoxSizer:add(MainSz, AppWn,  zxw:flags(wide)),
    ok = wxFrame:setSizer(Frame, MainSz),
    ok = wxSizer:layout(MainSz),

    ok =
        case safe_size() of
            {pref, max} ->
                wxTopLevelWindow:maximize(Frame);
            {pref, WSize} ->
                wxFrame:setSize(Frame, WSize);
            {center, WSize} ->
                ok = wxFrame:setSize(Frame, WSize),
                wxFrame:center(Frame)
        end,

    ok = wxFrame:connect(Frame, command_button_clicked),
    ok = wxFrame:connect(Frame, command_menu_selected),
    ok = wxFrame:connect(Frame, close_window),
    true = wxFrame:show(Frame),
    State = #s{wx = Wx, frame = Frame, main = MainSz,
               apps = AppWn, previous = Previous},
    {Frame, State}.

safe_size() ->
    Display = wxDisplay:new(),
    GSize = wxDisplay:getGeometry(Display),
    CSize = wxDisplay:getClientArea(Display),
    PPI =
        try
            wxDisplay:getPPI(Display)
        catch
            Class:Exception -> {Class, Exception}
        end,
    ok = log(info, "Geometry: ~p", [GSize]),
    ok = log(info, "ClientArea: ~p", [CSize]),
    ok = log(info, "PPI: ~p", [PPI]),
    Geometry =
        case file:consult(prefs_path()) of
            {ok, [{geometry, G}]} ->
                {pref, G};
            _ ->
                {X, Y, _, _} = GSize,           
                {center, {X, Y, 560, 520}}
        end,
    ok = wxDisplay:destroy(Display),
    Geometry.

prefs_path() ->
    filename:join(zx_lib:path(etc, "otpr", "vapor"), "preferences.conf").


-spec handle_call(Message, From, State) -> Result
    when Message  :: term(),
         From     :: {pid(), reference()},
         State    :: state(),
         Result   :: {reply, Response, NewState}
                   | {noreply, State},
         Response :: ok
                   | {error, {listening, inet:port_number()}},
         NewState :: state().

handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp~n", [From, Unexpected]),
    {noreply, State}.


-spec handle_cast(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The gen_server:handle_cast/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_cast-2

handle_cast({user_message, Contents}, State) ->
    ok = do_user_message(Contents, State),
    {noreply, State};
handle_cast({show, Type, Descriptions}, State) ->
    NewState = do_show(Type, Descriptions, State),
    {noreply, NewState};
handle_cast({update_versions, PackageID, Versions}, State) ->
    NewState = do_update_versions(PackageID, Versions, State),
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = log(warning, "Unexpected cast: ~tp~n", [Unexpected]),
    {noreply, State}.


-spec handle_info(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The gen_server:handle_info/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_info-2

handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.


-spec handle_event(Event, State) -> {noreply, NewState}
    when Event    :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The wx_object:handle_event/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_info-2

handle_event(#wx{id = ID, event = #wxCommand{type = command_button_clicked}},
             State = #s{frame = Frame, packages = Packages}) ->
    ok = pick(ID, Packages, Frame),
    {noreply, State};
handle_event(#wx{id = ID, event = #wxCommand{type = command_menu_selected}},
             State = #s{frame = Frame}) ->
    ok =
        case ID of
            ?fileIMP_REALM -> do_imp_realm(Frame);
            ?fileEXP_REALM -> do_exp_realm(Frame);
            ?fileREM_REALM -> do_rem_realm(Frame);
            ?toolOBSERVER  -> vw_con:run_observer();
            ?wxID_EXIT     -> close(Frame);
            _              -> tell("Received menu event ID: ~p", [ID])
        end,
    {noreply, State};
handle_event(#wx{event = #wxClose{}}, State = #s{frame = Frame}) ->
    ok = close(Frame),
    {noreply, State};
handle_event(Event, State) ->
    ok = log(info, "Unexpected event ~tp State: ~tp~n", [Event, State]),
    {noreply, State}.



code_change(_, State, _) ->
    {ok, State}.


terminate(Reason, State) ->
    ok = log(info, "Reason: ~tp, State: ~tp", [Reason, State]),
    wx:destroy().



%%% Doer Functions

close(Frame) ->
    Geometry =
        case wxTopLevelWindow:isMaximized(Frame) of
            true ->
                max;
            false ->
                {X, Y} = wxWindow:getPosition(Frame),
                {W, H} = wxWindow:getSize(Frame),
                {X, Y, W, H}
        end,
    Prefs = [{geometry, Geometry}],
    ok = zx_lib:write_terms(prefs_path(), Prefs),
    ok = vw_con:stop(),
    wxFrame:destroy(Frame).


do_user_message(Contents, #s{frame = Frame}) ->
    zxw:show_message(Frame, Contents).


do_show(Type, Descriptions, State = #s{previous = Previous}) ->
    Ordered = order(Previous, Descriptions),
    wx:batch(fun() -> do_show2(Type, Ordered, State) end).

order([PackageID | Rest], Descriptions) ->
    {Target, Remaining} = pluck(PackageID, Descriptions, []),
    [Target | order(Rest, Remaining)];
order([], Remaining) ->
    Remaining;
order(_, []) ->
    [].

pluck(P = {R, N, _}, [D | Ds], Acc) ->
    case element(2, D) of
        {R, N, _} -> {D, lists:reverse(Acc, Ds)};
        _         -> pluck(P, Ds, [D | Acc])
    end.

do_show2(Type, Descriptions, State = #s{frame = Frame, main = MainSz, apps = OldAppWn})
        when Type == gui; Type == app ->
    ok = wxScrolledWindow:destroy(OldAppWn),
    AppWn = wxScrolledWindow:new(Frame),
    AppSz = wxBoxSizer:new(?wxVERTICAL),
    Add =
        fun
            ({description, {"otpr", "vapor", _}, _, _, _, _, _, _, _, _}, Ps) ->
                Ps;
            (Description, Ps) ->
                PackageSz = wxBoxSizer:new(?wxHORIZONTAL),
                Style = [{style, ?wxHORIZONTAL}, {size, {450, 3}}],
                Line = wxStaticLine:new(AppWn, Style),
                {DetailSz, PackageID} = detail(Description, AppWn),
                _ = wxBoxSizer:add(PackageSz, DetailSz, zxw:flags(wide)),
                P = add_launcher(PackageID, PackageSz, AppWn),
                _ = wxBoxSizer:add(AppSz, Line),
                _ = wxBoxSizer:add(AppSz, PackageSz, zxw:flags(wide)),
                _ = wxBoxSizer:addSpacer(AppSz, 15),
                [P | Ps]
        end,
    Packages = lists:foldl(Add, [], Descriptions),
    ok = wxScrolledWindow:setSizerAndFit(AppWn, AppSz),
    ok = wxScrolledWindow:setScrollRate(AppWn, 5, 5),
    _ = wxBoxSizer:add(MainSz, AppWn, zxw:flags(wide)),
    ok = wxBoxSizer:layout(MainSz),
    State#s{apps = AppWn, packages = Packages};
do_show2(Type, Descriptions, State)
        when Type == cli; Type == lib ->
    do_show3(Descriptions, State).

do_show3(Descriptions, State = #s{frame = Frame, main = MainSz, apps = OldAppWn}) ->
    ok = wxScrolledWindow:destroy(OldAppWn),
    AppWn = wxScrolledWindow:new(Frame),
    AppSz = wxBoxSizer:new(?wxVERTICAL),
    Add =
        fun(Description, Ps) ->
            PackageSz = wxBoxSizer:new(?wxHORIZONTAL),
            Line = wxStaticLine:new(AppWn, [{style, ?wxHORIZONTAL}, {size, {450, 3}}]),
            {DetailSz, PackageID} = detail(Description, AppWn),
            _ = wxBoxSizer:add(PackageSz, DetailSz, zxw:flags(wide)),
            _ = wxBoxSizer:add(AppSz, Line),
            _ = wxBoxSizer:add(AppSz, PackageSz, zxw:flags(wide)),
            _ = wxBoxSizer:addSpacer(AppSz, 15),
            P = #package{id = PackageID},
            [P | Ps]
        end,
    Packages = lists:foldl(Add, [], Descriptions),
    ok = wxScrolledWindow:setSizerAndFit(AppWn, AppSz),
    ok = wxScrolledWindow:setScrollRate(AppWn, 5, 5),
    _ = wxBoxSizer:add(MainSz, AppWn, zxw:flags(wide)),
    ok = wxBoxSizer:layout(MainSz),
    State#s{apps = AppWn, packages = Packages}.


add_launcher(PackageID, PackageSz, AppWn) ->
    LaunchSz = wxBoxSizer:new(?wxVERTICAL),
    LaunchBn = wxButton:new(AppWn, ?wxID_ANY, [{label, "Run"}, {size, {100, 100}}]),
    LaunchID = wxButton:getId(LaunchBn),
    _ = wxBoxSizer:add(LaunchSz, LaunchBn),
    {VsnPicker, VsnButton} =
        case element(3, PackageID) of
            {0, 0, 0} ->
                {none, none};
            Version ->
                {ok, VersionString} = zx_lib:version_to_string(Version),
                VsnPickerBn = wxButton:new(AppWn, ?wxID_ANY, [{label, VersionString}]),
                VsnPickerID = wxButton:getId(VsnPickerBn),
                _ = wxBoxSizer:add(LaunchSz, VsnPickerBn, zxw:flags(base)),
                {{button, VsnPickerID}, VsnPickerBn}
        end,
    _ = wxBoxSizer:add(PackageSz, LaunchSz, zxw:flags(base)),
    #package{id         = PackageID,
             launch_id  = LaunchID,
             vsn_picker = VsnPicker,
             vsn_button = VsnButton}.

detail({description,
        PackageID, DName, _, Desc, Author, AEmail, WebURL, RepoURL, _},
       AppWn) ->
    {ok, PackageString} = zx_lib:package_string(PackageID),
    DescSz = wxBoxSizer:new(?wxVERTICAL),
    NameText = wxStaticText:new(AppWn, ?wxID_ANY, DName),
    Big = wxFont:new(20, ?wxMODERN, ?wxNORMAL, ?wxNORMAL, [{face, "Sans"}]),
    true = wxStaticText:setFont(NameText, Big),
    DescText = wxStaticText:new(AppWn, ?wxID_ANY, Desc),
    _ = wxBoxSizer:add(DescSz, NameText),
    _ = wxBoxSizer:add(DescSz, DescText),
    DetailSz = wxFlexGridSizer:new(4, 2, 5, 5),
    ok = add_detail("Author", format(Author, AEmail), DetailSz, AppWn),
    ok = add_detail("Package", PackageString, DetailSz, AppWn),
    ok = add_detail("Web", WebURL, DetailSz, AppWn),
    ok = add_detail("Repo", RepoURL, DetailSz, AppWn),
    _ = wxBoxSizer:add(DescSz, DetailSz, zxw:flags(wide)),
    {DescSz, PackageID}.

add_detail(Label, String, DetailSz, AppWn) ->
    LabelText = wxStaticText:new(AppWn, ?wxID_ANY, Label),
    StringText = wxStaticText:new(AppWn, ?wxID_ANY, String),
    _ = wxFlexGridSizer:add(DetailSz, LabelText),
    _ = wxFlexGridSizer:add(DetailSz, StringText),
    ok.

format("", "")        -> "";
format(Author, "")    -> Author;
format("", Email)     -> "<" ++ Email ++ ">";
format(Author, Email) -> Author ++ " <" ++ Email ++">".



pick(Selected, Packages, Frame) ->
    case lists:keyfind(Selected, #package.launch_id, Packages) of
        #package{id = PackageID, vsn_picker = {button, _}} ->
            ok = vw_con:launch(PackageID),
            wxFrame:destroy(Frame);
        #package{id = PackageID, vsn_picker = none} ->
            ok = vw_con:launch(PackageID),
            wxFrame:destroy(Frame);
        #package{id = PackageID, vsn_picker = VsnPicker} ->
            VS = wxChoice:getString(VsnPicker, wxChoice:getSelection(VsnPicker)),
            {ok, Version} = zx_lib:string_to_version(VS),
            NewPackageID = setelement(3, PackageID, Version),
            ok = vw_con:launch(NewPackageID),
            wxFrame:destroy(Frame);
        false ->
            case lists:keyfind({button, Selected}, #package.vsn_picker, Packages) of
                #package{id = PackageID} ->
                    vw_con:list_versions(PackageID);
                false ->
                    tell(info, "Oh noes! Package doesn't exist!")
            end
    end.


do_imp_realm(Frame) ->
    FileDialog = wxFileDialog:new(Frame, [{wildCard, "*.zrf"}]),
    _ = wxFileDialog:showModal(FileDialog),
    case wxFileDialog:getPath(FileDialog) of
        ""   -> ok;
        Path -> do_imp_realm(Frame, Path)
    end.

do_imp_realm(Frame, Path) ->
    case zx_local:import_realm(Path) of
        ok ->
            ok;
        {error, Reason, _} ->
            Options = [{caption, "Error"}, {style, ?wxICON_INFORMATION}],
            Dialog = wxMessageDialog:new(Frame, Reason, Options),
            _ = wxMessageDialog:showModal(Dialog),
            wxMessageDialog:destroy(Dialog)
    end.


do_exp_realm(Frame) ->
    case pick_realm(Frame) of
        none  -> ok;
        ""    -> ok;
        Realm -> do_exp_realm2(Frame, Realm)
    end.

do_exp_realm2(Frame, Realm) ->
    ZRF = Realm ++ ".zrf",
    Dialog = wxFileDialog:new(Frame, [{style, ?wxFD_SAVE}, {defaultFile, ZRF}]),
    _ = wxFileDialog:showModal(Dialog),
    case wxFileDialog:getPath(Dialog) of
        ""   -> ok;
        Path -> do_exp_realm3(Realm, Path)
    end.

do_exp_realm3(Realm, Path) ->
    {ok, RealmConf} = zx_lib:load_realm_conf(Realm),
    KeyName = maps:get(key, RealmConf),
    {ok, PubDER} = zx_daemon:get_keybin(public, {Realm, KeyName}),
    Blob = term_to_binary({RealmConf, PubDER}),
    ok = file:write_file(Path, Blob),
    tell("Realm conf file written to ~ts", [Path]).


do_rem_realm(Frame) ->
    case pick_realm(Frame) of
        none  -> ok;
        ""    -> ok;
        Realm -> do_rem_realm2(Frame, Realm)
    end.

do_rem_realm2(Frame, Realm) ->
    Format =
        "REALM REMOVAL CONFIRMATION\n\n"
        "Realm: ~tp\n\n"
        "Are you 100% certain that you want to remove this realm?\n\n"
        "If you need this realm in the future you will need to import it again. "
        "To import the realm you will need its realm file (.zrf file). "
        "You can export this realm first to ensure you can restore the previous state "
        "by selecting \"Export Realm\" from the file menu before removing it.",
    Message = io_lib:format(Format, [Realm]),
    Options =
        [{caption, "WARNING"},
         {style, ?wxOK bor ?wxCANCEL}],
    Dialog = wxMessageDialog:new(Frame, Message, Options),
    case wxMessageDialog:showModal(Dialog) of
        ?wxID_CANCEL -> ok;
        ?wxID_OK     -> zx_daemon:drop_realm(Realm)
    end.


pick_realm(Frame) ->
    {ok, Realms} = zx:list(),
    Dialog = wxDialog:new(Frame, ?wxID_ANY, "Pick a Realm"),
    Sizer = wxBoxSizer:new(?wxVERTICAL),
    PickerOptions = [{choices, Realms}, {style, ?wxLB_SINGLE}],
    Picker = wxListBox:new(Dialog, ?wxID_ANY, PickerOptions),
    ButtonSz = wxDialog:createButtonSizer(Dialog, ?wxOK bor ?wxCANCEL),
    _ = wxBoxSizer:add(Sizer, Picker, [{flag, ?wxEXPAND}, {proportion, 1}]),
    _ = wxBoxSizer:add(Sizer, ButtonSz),
    ok = wxDialog:setSizer(Dialog, Sizer),
    Result =
        case wxDialog:showModal(Dialog) of
            ?wxID_OK     -> wxListBox:getStringSelection(Picker);
            ?wxID_CANCEL -> none
        end,
    ok = wxDialog:destroy(Dialog),
    Result.


do_update_versions(PackageID, Versions, State) ->
    wx:batch(fun() -> do_update_versions2(PackageID, Versions, State) end).

do_update_versions2(PackageID,
                    Versions,
                    State = #s{main = MainSz, apps = AppWn, packages = Packages}) ->
    NewState =
        case lists:keyfind(PackageID, #package.id, Packages) of
            P = #package{vsn_picker = {button, _}, vsn_button = Button} ->
                LaunchSz = wxButton:getContainingSizer(Button),
                VersionStrings = versions_to_strings(Versions),
                VsnPicker = wxChoice:new(AppWn, ?wxID_ANY, [{choices, VersionStrings}]),
                ok = wxChoice:setSelection(VsnPicker, 0),
                true = wxBoxSizer:replace(LaunchSz, Button, VsnPicker),
                ok = wxButton:destroy(Button),
                NewP = P#package{vsn_picker = VsnPicker, vsn_button = none},
                NewPackages = lists:keystore(PackageID, #package.id, Packages, NewP),
                State#s{packages = NewPackages};
            #package{vsn_picker = VsnPicker} ->
                VersionStrings = versions_to_strings(Versions),
                ok = zxChoice:clear(VsnPicker),
                ok = zxChoice:appendStrings(VsnPicker, VersionStrings),
                ok = wxChoice:setSelection(VsnPicker, 0),
                State;
            false ->
                ok = log(error, "Received version update for non-existant package"),
                State
        end,
    ok = wxBoxSizer:layout(MainSz),
    NewState.

versions_to_strings(Versions) ->
    Stringify = fun(V) -> {ok, S} = zx_lib:version_to_string(V), S end,
    lists:reverse(lists:map(Stringify, lists:sort(Versions))).
