%%% @doc
%%% Vapor Controller
%%%
%%% This process is a in charge of maintaining the program's state.
%%% @end

-module(vw_con).
-vsn("0.2.4").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-behavior(gen_server).
-export([show/1, list_versions/1, launch/1, run_observer/0]).
-export([start_link/0, stop/0]).
-export([init/1, terminate/2, code_change/3,
         handle_call/3, handle_cast/2, handle_info/2]).
-include("$zx_include/zx_logger.hrl").


%%% Type and Record Definitions


-record(s,
        {window   = none       :: none | wx:wx_object(),
         previous = previous() :: [zx:package_id()],
         observer = none       :: none | {pid(), reference()}}).

-type state() :: #s{}.



%% Interface

-spec show(Type) -> ok
    when Type :: zx:package_type().

show(Type) ->
    gen_server:cast(?MODULE, {show, Type}).


list_versions(PackageID) ->
    gen_server:cast(?MODULE, {list_versions, PackageID}).


launch(PackageID) ->
    gen_server:cast(?MODULE, {launch, PackageID}).


run_observer() ->
    gen_server:cast(?MODULE, run_observer).


%%% Start/Stop

-spec start_link() -> Result
    when Result :: {ok, pid()}
                 | {error, Reason},
         Reason :: {already_started, pid()}
                 | {shutdown, term()}
                 | term().
%% @private
%% Called by vw_sup.

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).


-spec stop() -> ok.

stop() ->
    gen_server:cast(?MODULE, stop).




-spec init(none) -> no_return().

init(none) ->
    ok = log(info, "Starting"),
    State = #s{previous = Previous} = #s{},
    Window = vw_gui:start_link("Vapor", Previous),
    ok = log(info, "Window: ~p", [Window]),
    {ok, State#s{window = Window}}.

previous() ->
    Path = previous_path(),
    ok = filelib:ensure_dir(Path),
    case file:consult(Path) of
        {ok, Previous}  -> Previous;
        {error, enoent} -> []
    end.

previous_path() ->
    filename:join(zx_lib:path(var, "otpr", "vapor"), "previous.eterms").


%%% gen_server Message Handling Callbacks


-spec handle_call(Message, From, State) -> Result
    when Message  :: term(),
         From     :: {pid(), reference()},
         State    :: state(),
         Result   :: {reply, Response, NewState}
                   | {noreply, State},
         Response :: ok
                   | {error, {listening, inet:port_number()}},
         NewState :: state().
%% @private
%% The gen_server:handle_call/3 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_call-3

handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp~n", [From, Unexpected]),
    {noreply, State}.


-spec handle_cast(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The gen_server:handle_cast/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_cast-2

handle_cast({show, Type}, State) ->
    ok = do_show(Type),
    {noreply, State};
handle_cast({list_versions, PackageID}, State) ->
    ok = do_list_versions(PackageID),
    {noreply, State};
handle_cast({launch, PackageID}, State) ->
    NewState = do_launch(PackageID, State),
    {noreply, NewState};
handle_cast(run_observer, State) ->
    NewState = do_run_observer(State),
    {noreply, NewState};
handle_cast(stop, State) ->
    ok = log(info, "Received a 'stop' message."),
    {stop, normal, State};
handle_cast(Unexpected, State) ->
    ok = log(warning, "Unexpected cast: ~tp~n", [Unexpected]),
    {noreply, State}.


-spec handle_info(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The gen_server:handle_info/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_info-2

handle_info({'DOWN', Mon, process, Pid, Info}, State = #s{observer = {Pid, Mon}}) ->
    ok = log(info, "Observer went down with ~p", [Info]),
    {noreply, State#s{observer = none}};
handle_info(Unexpected, State) ->
    ok = log(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.


%%% do_* Functions

do_show(Type) ->
    {ok, Packages} = zx:list_type_ar(Type),
    {ok, Descriptions} = zx:describe_plural(Packages),
    vw_gui:show(Type, Descriptions).


do_list_versions(PackageID = {Realm, Name, _}) ->
    {ok, ID} = zx_daemon:list(Realm, Name),
    {ok, Versions} = zx_daemon:wait_result(ID),
    vw_gui:update_versions(PackageID, Versions).


do_run_observer(State = #s{observer = none}) ->
    Observer =
        case whereis(observer) of
            undefined ->
                ok = observer:start(),
                whereis(observer);
            Pid ->
                Pid
        end,
    Mon = monitor(process, Observer),
    State#s{observer = {Observer, Mon}};
do_run_observer(State) ->
    ok = tell(info, "Cannot launch observer twice."),
    State.


do_launch(PackageID, State = #s{previous = Previous}) ->
    NewPrevious = restack(Previous, PackageID),
    ok = zx_lib:write_terms(previous_path(), NewPrevious),
    {ok, PackageString} = zx_lib:package_string(PackageID),
    ok = zx:not_done(zx:run(PackageString, [])),
    State#s{previous = NewPrevious}.

restack(Previous, PackageID = {Realm, Name, _}) ->
    Scrubbed = scrub(Previous, Realm, Name),
    [PackageID | Scrubbed].

scrub([{R, N, _} | Rest], R, N) -> Rest;
scrub([P | Rest], R, N)         -> [P | scrub(Rest, R, N)];
scrub([], _, _)                 -> [].


%% @private
%% gen_server callback to handle state transformations necessary for hot
%% code updates. This template performs no transformation.

code_change(_, State, _) ->
    {ok, State}.


terminate(Reason, State) ->
    ok = log(info, "Reason: ~tp, State: ~tp", [Reason, State]),
    zx:stop().
