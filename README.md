# Vapor

A graphical application browser and front-end for ZX/Zomp.

Vapor requires that [ZX](https://zxq9.com/projects/zomp/download.en.html) be installed.

To run Vapor from the command line on a normal OS: `zx run vapor`.

On Windows click the desktop icon named "Vapor" after running the installer. (Note that Vapor is the primary way of running ZX based Erlang apps on Windows, as the command line is a bit clunky there.)


# Attribution

## Icons

The "vapor" project icon was produced by a super nice and talented guy named
Adrien Coquet and is used with permission under the Creative Commons
Attribution v3.0 license.

Check out Adrien's work on The Noun Project here:
https://thenounproject.com/coquet_adrien/

Visit Adrien's personal site here:
https://adrien-coquet.com/
